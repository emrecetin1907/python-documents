cumle = "Bugün hava yağmurlu"

"""
    NOT: Indeks değerleri her zaman sıfırdan başlar!!!
"""
# print(cumle[2]) #2. indeksteki değeri yazdırır.
# print(cumle[2:12]) #2. indeksten 12. indekse kadar olan kısmı yazdırır. 12. indeks hariç!
# print(cumle[12])
# print(cumle[2:3])
# print(cumle[2:12:2]) #2.indeksten başla 2 şer 2 şer git
# print(cumle[2:12:3]) #2.indeksten başla 3 er 3 er git
# print(cumle[12:2:-1]) #1 er 1 er geri git
# print(cumle[12:2:-2]) #2 şer 2 şer geri git
# print(cumle[2:]) #2'den başla sona kadar git
# print(cumle[:12]) #0'dan başla 12.indekse kadar git
# print(cumle[:]) #Baştan sona yazar
# print(cumle[2::2])
# print(cumle[2:12:-1])



# print(cumle.upper()) #bütün karakterleri büyük harfe çevirir.
# print(cumle.lower()) #bütün karakterleri küçükharfe çevirir.
# print(cumle.upper().lower()) #önce büyütüp sonra küçülttük
# print(cumle.lower().capitalize()) #önce hepsini küçültüp ardından baş harfi büyüttük.
# print(cumle.title()) #Hepsinin ilk harfini büyütür.
# #print(cumle.title().swapcase())
# yeniCumle = cumle.title()
# print(yeniCumle)
# print(yeniCumle.swapcase()) #büyükleri küçük, küçükleri büüyk yapar
# print(cumle.replace("a","ü")) #değiştirmek istediğimiz karakterin yerine farklı bir karakter koyduk
# print(cumle.replace("hava","gökyüzü"))
#
#
#
# print(cumle.index("h")) #girdiğimiz değerin kaçıncı indekste olduğunu gösterir
# cumle = ",Bugün hava yağmurlu,,,,"
# print(cumle)
# print(cumle.rstrip()) #sağdaki boşlukları siler
# print(cumle.lstrip()) #soldaki boşlukları siler
# print(cumle.strip()) #sağdaki ve soldaki boşlukları aynı anda siler.
# print(cumle.strip(","))
#
# cumle = "bugün hava yağmurlu"
# print(cumle.endswith("u")) #bu değer ile mi bitiyor?
# print(cumle.endswith("f"))
# print(cumle.endswith("yağmurlu"))
# print(cumle.startswith("B")) #bu değer ile mi başlıyor?
# print(cumle.startswith("b"))
#
# print(cumle.isalpha()) #harflerden mi oluşuyor?
# print(cumle.isdigit()) #sayılardan mı oluşuyor
# print(cumle.isalnum()) #alfabetik veya nümerik karakterlerden oluşuyorsa true döner değilse false döner.
# print(cumle.isspace()) #boşluktan mı oluşuyor
# print(cumle.isidentifier())


isim = "Ali"
soyisim = "Kara"
numara = "1234"
print("İsim: " + isim + " Soyisim: " + soyisim + " Numara: " + numara)
print("İsim: {}, Soyisim: {}, Numara: {}".format(isim,soyisim,numara,isim))


















