
# print(type("kucukkayasidar@gmail.com")) #type()fonksiyonu kendisine verilen parametrelerin tipini belirler.
# print(type("asdasdasdasasd"))
# print(len("kucukkayasidar@gmail.com")) #len() fonksiyonu kendisine verilen parametrenin uzunluğunu verir.
# print(len("asdasdasdasdas"))
# print(len("kucukkayasidar@gmail.com") + len("asdasdasdas"))
# print(len("123123123123123"))
#
#
# sayi1 = 10 #int değer ataması
# sayi2 = 20
# print(sayi1)
# print(sayi2)
# print(sayi1 + sayi2)
# print(sayi1 * 5)
# isim = "Veli"
# soyisim = "Ağaç"
# print(isim)
# print(soyisim)
# print(isim + " " + soyisim)
# print(isim * sayi1)
#
# kullaniciAdi = "kucukkayasidar@gmail.com"
# sifre = "asdasdasdas"
# print(len(kullaniciAdi))
# print(len(sifre))
# print(len(kullaniciAdi) + len(sifre))



"""
    Bir dairenin alanını hesaplayan programı yazın. (pi*r^2)
"""

# cap = 10
# yaricap = cap / 2
# pi = 3.14
# #print(pi * (yaricap * yaricap))
# alan = pi * (yaricap * yaricap)
# print(alan)
# #print(math.pi)
# alan = pi * pow(yaricap,2)
# print(alan)
# karekok = pow(9,0.5)
# print(karekok)


"""
    Değişken Adı Belirleme Kuralları
1. Değişken adları kesinlikle sayı ile başlayamaz.
2. Değişken adlar aritmetik operatörlerle başlayamaz.
3. Değişken adları ya bir alfabe harfiyle ya da "_" ile başlamalıdır.
4. Değişkenlere fonksiyon adları verilemez. verildiği takdirde fonksiyon olmaktan çıkar.
5. Değiken oluştururken kelimeler arasında kesinlikle boşluk bırakılamaz
"""
# 1sayi = 10
# +deger = 1231
# print(+deger)
# _sayi = 5
# print(_sayi)
# len = 10
# print(len)
# print(len("asdasdas"))
#kullanici adi = "asdasd"
# type = "tip"
# print(type)
# print(type("asdasdasdasdasd"))


a = 5
b = 10
c = 20
d = 20

x, y, z = 1,2,3 #tek satırda değişken yazma
x = 5
print(x)

isim1, isim2, isim3 = 37,"ahmet,1",4.3
print(isim1)