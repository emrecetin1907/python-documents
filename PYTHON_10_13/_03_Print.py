#region PRINT
# print("Python")
# print('Python')
# print("""Python""")
#endregion

#region TIRNAK KULLANIMI
# print("Bana 'Baba' dedi.")
# #print("Bana "Baba" dedi.")
# print('Bana "Baba" dedi.')
# print("""Bana "Baba" dedi.""")
# #print("Bana """Baba""" dedi.")


# print("""Merhaba
# Nasılsın?""")
# print("Merhaba"
#       "Nasılsın")
# print("""A: Merhaba
# B: Merhaba
# A: Nasılsın""")
#endregion

#region VİRGÜL KULLANIMI
# print("Ahmet" + " " + "Avcı") #print() fonksiyonu tıpkı pow fonksiyonu gibi birden fazla parametre alabilir.
# print("Ahmet","Avcı")
# print("Ahmet","Avcı",1982,"İstanbul")
#endregion

#region SEP END OPERATÖRLERİ
# print("www.","ucuncubinyil",".com",sep="") #seperator: ayraç
# print("Ahmet","Avcı",1982,"İstanbul",sep=" - ")
# print("Merhaba","Naber",sep="?")
# #print("Nasılsın","İyi misin?",sep=3)
# print(1,2,3,4,80.77,"asdasdasd",sep="asdasdas")
# print("Ahmet","Ali",27,sep="-",end="?")
# print("asdasdasdas")
#endregion

#region KAÇIŞ KARAKTERİ
print("""birinci satır
ikinci satır
üçüncü satır""")
print("birinci satır\nikinci satır\nüçüncü satır") #\n: kaçış karakteri
#endregion

#region YILDIZLI PARAMETRELER
print("M","e","r","h","a","b","a",sep=".")
print(*"Merhaba",sep=".")
print(*"Merhaba")
#endregion

